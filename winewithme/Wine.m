//
//  Wine.m
//  winewithme
//
//  Created by Keith Elliott on 1/21/13.
//  Copyright (c) 2013 Keith Elliott. All rights reserved.
//

#import "Wine.h"

@implementation Wine
@synthesize name;
@synthesize region;
@synthesize year;
@synthesize rating;
@synthesize type;
@end
