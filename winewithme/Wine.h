//
//  Wine.h
//  winewithme
//
//  Created by Keith Elliott on 1/21/13.
//  Copyright (c) 2013 Keith Elliott. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Wine : NSObject
@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSString *region;
@property(strong, nonatomic) NSString *year;
@property int rating;
@property int type;
@end
