//
//  WineTableViewController.h
//  winewithme
//
//  Created by Keith Elliott on 1/21/13.
//  Copyright (c) 2013 Keith Elliott. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Wine;

@interface WineTableViewController : UITableViewController
@property (strong, nonatomic) NSMutableArray *wineRatings;
@end
